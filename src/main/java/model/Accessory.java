package model;

public class Accessory {
    private String accessory;

    public Accessory() {
    }

    public Accessory(String accessory) {
        this.accessory = accessory;
    }

    public String getAccessoryName() {
        return accessory;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessory = accessory;
    }

}
