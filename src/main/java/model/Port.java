package model;

public class Port {
    private String portName;

    public Port() {
    }

    public Port(String portName) {
        this.portName = portName;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }
}
